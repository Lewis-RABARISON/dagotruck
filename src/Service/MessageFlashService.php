<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MessageFlashService extends AbstractController
{
    /**
     * Message flash
     *
     * @param string $entity_title
     * @param string $message
     * @return void
     */
    public function messageFlash($entity_title,$message)
    {
        $this->addFlash('success',$entity_title.' '.$message.' avec succès');
    }
}