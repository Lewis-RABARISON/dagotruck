<?php

namespace App\Service;

use App\Repository\DetailMaterielRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CartService 
{
    private $session;
    private $detailMaterielRepository;
    
    public function __construct(SessionInterface $session,DetailMaterielRepository $detailMaterielRepository)
    {
        $this->session = $session;
        $this->detailMaterielRepository = $detailMaterielRepository;
    }
    
    public function add(int $id)
    {
        $panier = $this->session->get('panier',[]);
        if(!empty($panier[$id])){
            $panier[$id]++;
        }
        else{
            $panier[$id] = 1;
        }
        $this->session->set('panier',$panier);
    }

    public function decreaseQuantity(int $id)
    {
        $panier = $this->session->get('panier',[]);
        if(!empty($panier[$id])){
            if($panier[$id] > 0){
                $panier[$id]--;
            }
        }
        $this->session->set('panier',$panier);
    }

    public function remove(int $id)
    {
        $panier = $this->session->get('panier',[]);
        if(!empty($panier[$id])){
            unset($panier[$id]);
        }
        $this->session->set('panier',$panier);
    }

    /**
     * Obtient la liste des détails matériels dans le panier
     * @return array
     */
    public function getFullCart():array
    {
        $panier = $this->session->get('panier',[]);
        
        foreach ($panier as $id => $quantity) {
            $panierWithData[] = [
                'materiel' => $this->detailMaterielRepository->find($id),
                'quantity' => $quantity
            ]; 
        }

        if (empty($panierWithData)) {
            return [];
        }

        return $panierWithData;
    }

    /**
     * Obtient le total du détail matériel dans le panier
     * @return float
     */
    public function getTotal():float
    {
        $total = 0;

        foreach ($this->getFullCart() as $item) {
            if ($item['materiel'] != NULL){
                $totalItem = $item['materiel']->getPrixUnitaire() *  $item['quantity'];
            } else{
                $totalItem = 0;
            }
            $total += $totalItem;
        }

        return $total;
    }
}

