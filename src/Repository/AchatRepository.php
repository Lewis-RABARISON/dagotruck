<?php

namespace App\Repository;

use App\Entity\Achat;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Achat|null find($id, $lockMode = null, $lockVersion = null)
 * @method Achat|null findOneBy(array $criteria, array $orderBy = null)
 * @method Achat[]    findAll()
 * @method Achat[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AchatRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, Achat::class);
        $this->em = $em;
    }

    // /**
    //  * @return Achat[] Returns an array of Achat objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Achat
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Get all purchase sorted by day
    */
    public function getPurchaseByDay($data_achat_journalier)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT achat.id AS achat_id, achat.*,detail_materiel.*,departement.* FROM `achat_detail_materiel` 
                INNER JOIN achat ON achat.id = achat_detail_materiel.achat_id 
                INNER JOIN detail_materiel ON detail_materiel.id = achat_detail_materiel.detail_materiel_id 
                INNER JOIN departement ON departement.id = detail_materiel.departement_id
                WHERE achat.date_achat = :data_achat_journalier
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['data_achat_journalier' => $data_achat_journalier]);

        return $stmt->fetchAllAssociative();
    }

    /**
     * Get all purchase sorted by year
    */
    public function getPurchaseByYear($data_achat_annuel)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT achat.id AS achat_id, achat.*,detail_materiel.*,departement.* FROM `achat_detail_materiel` 
                INNER JOIN achat ON achat.id = achat_detail_materiel.achat_id 
                INNER JOIN detail_materiel ON detail_materiel.id = achat_detail_materiel.detail_materiel_id 
                INNER JOIN departement ON departement.id = detail_materiel.departement_id
                WHERE YEAR(achat.date_achat) = :data_achat_annuel
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['data_achat_annuel' => $data_achat_annuel]);

        return $stmt->fetchAllAssociative();
    }

    /**
     * Get nom acheteur
     */
    public function nomAcheteur()
    {
        $query = $this->em->createQuery(
            'SELECT DISTINCT(a.nomAcheteur) AS nomAcheteur
            FROM App\Entity\Achat a
            WHERE a.typePaiement = :type
           '
        )->setParameter('type', "Abonné");

        return $query->getResult();
    }

    /**
     * Get all purchase sorted by year and month
    */
    public function getPurchaseByYearMonth($data_achat_mensuel)
    {
        $conn = $this->getEntityManager()->getConnection();
        $year = $data_achat_mensuel->format("Y"); 
        $month = $data_achat_mensuel->format("m"); 

        $sql = '
                SELECT achat.id AS achat_id, achat.*,detail_materiel.*,departement.* FROM `achat_detail_materiel` 
                INNER JOIN achat ON achat.id = achat_detail_materiel.achat_id 
                INNER JOIN detail_materiel ON detail_materiel.id = achat_detail_materiel.detail_materiel_id 
                INNER JOIN departement ON departement.id = detail_materiel.departement_id
                WHERE YEAR(achat.date_achat) = :year AND MONTH(achat.date_achat) = :month
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(compact("year","month"));

        return $stmt->fetchAllAssociative();
    }

    // SELECT DISTINCT(YEAR(`date_achat`)) AS year FROM `achat` ORDER BY `date_achat` DESC

    public function yearPurchase()
    {
        $query = $this->em->createQuery(
            'SELECT (a.dateAchat) AS dateAchat
            FROM App\Entity\Achat a
            ORDER BY dateAchat DESC
           '
        );

        return $query->getResult();
    }

    public function getAllAbonne()
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
                SELECT DISTINCT `nom_acheteur`, `code_facture` 
                FROM `achat` 
                WHERE `type_paiement`= :type_paiement
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['type_paiement' => "Abonné"]);

        return $stmt->fetchAllAssociative();
    }
}
