<?php

namespace App\Repository;

use App\Entity\DetailMateriel;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method DetailMateriel|null find($id, $lockMode = null, $lockVersion = null)
 * @method DetailMateriel|null findOneBy(array $criteria, array $orderBy = null)
 * @method DetailMateriel[]    findAll()
 * @method DetailMateriel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DetailMaterielRepository extends ServiceEntityRepository
{
    private $em;
    public function __construct(ManagerRegistry $registry, EntityManagerInterface $em)
    {
        parent::__construct($registry, DetailMateriel::class);
        $this->em = $em;
    }

    // /**
    //  * @return DetailMateriel[] Returns an array of DetailMateriel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DetailMateriel
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllMaterialsDetail()
    {
        return $this->createQueryBuilder('d')
                    ->select('d','dp.nomDepartement')
                    ->leftJoin('d.departement','dp')
                    ->getQuery()
                    ->execute();
    }

    public function getPaginatedMaterialDetail($page, $limit, $material_detail_research_value)
    {
        return $this->createQueryBuilder('d')
            ->orderBy('d.id', 'ASC')
            ->andWhere('d.reference LIKE :material_detail_research_value OR d.nom_materiel LIKE :material_detail_research_value 
                                OR d.categorie LIKE :material_detail_research_value 
                                OR d.qte LIKE :material_detail_research_value
                                OR d.prixUnitaire LIKE :material_detail_research_value
                                OR dp.nomDepartement LIKE :material_detail_research_value')
            ->setParameter('material_detail_research_value', '%'.$material_detail_research_value.'%')
            ->leftJoin('d.departement','dp')
            ->setMaxResults($limit)
            ->setFirstResult(($page * $limit) - $limit)
            ->getQuery()
            ->getResult()
        ;
    }

    public function getTotalMaterialDetail()
    {
        return $this->createQueryBuilder('d')
            ->select('COUNT(d)')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
