<?php

namespace App\Form;

use App\Entity\Departement;
use App\Entity\DetailMateriel;
use App\Entity\Materiel;
use App\Entity\TypeMateriel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DetailMaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('reference',TextType::class,[])
            ->add('nom_materiel',TextType::class,[
                "label" => "Nom du détail matériel"
            ])
            ->add('categorie',ChoiceType::class,[
                "choices" => [
                    'Camion' => "Camion",
                    'Minibus' => "Minibus",
                    'Léger' => "Léger"
                ]
            ])
            ->add('qte',IntegerType::class,[
                "label" => "Quantité",
                "attr" => [
                    "min" => 0
                ]
            ])
            ->add('prixUnitaire',IntegerType::class,[
                "label" => "Prix unitaire",
            ])
            ->add('typeMateriel',EntityType::class,[
                "class"        => TypeMateriel::class,
                "choice_label" => "descriptionType",
                "label"        => "Type de matériel",
                "mapped"       => false,
                "placeholder"  => "--Veuillez selectionner--",
                "required"     => false,
                "attr" => [
                    "class" => "select-two"
                ]
            ])
            ->add('materiel',EntityType::class,[
                "class" => Materiel::class,
                "choice_label" => "nomMarque",
                "placeholder" => "--Veuillez selectionner--",
                "attr" => [
                    "class" => "select-two"
                ]
            ])
            ->add('departement',EntityType::class,[
                "class" => Departement::class,
                "choice_label" => "nomDepartement",
                "placeholder" => "--Veuillez selectionner--",
                "attr" => [
                    "class" => "select-two"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => DetailMateriel::class,
        ]);
    }
}
