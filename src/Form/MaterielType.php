<?php

namespace App\Form;

use App\Entity\Materiel;
use App\Entity\TypeMateriel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MaterielType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomFournisseur',TextType::class,[
                "label" => "Nom du fournisseur"
            ])
            ->add('nomMarque',TextType::class,[
                "label" => "Nom/Marque"
            ])
            ->add('typeMateriel',EntityType::class,[
                "class" => TypeMateriel::class,
                "choice_label" => "descriptionType",
                "label" => "Type de matériel",
                "placeholder" => "--Veuillez selectionner--",
                "attr" => [
                    "class" => "select-two"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Materiel::class,
        ]);
    }
}
