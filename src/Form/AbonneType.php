<?php

namespace App\Form;

use App\Entity\Achat;
use App\Entity\DetailMateriel;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AbonneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateEcheance',DateType::class,[
                "widget" => "single_text",
                'attr' => [
                    "class" => "echeance"
                ]
            ])
            ->add('qteAchete',IntegerType::class,[
                "label" => "Quantité",
                'attr' => [
                    "class" => "quantity"
                ]
            ])
            ->add('avance',IntegerType::class,[
                'attr' => [
                    "class" => "avance"
                ]
            ])
            ->add('detailMateriel',EntityType::class,[
                "class" => DetailMateriel::class,
                "choice_label" => "nom_materiel",
                "label" => "Détail matériel",
                "mapped" => false,
                'attr' => [
                    "class" => "materiel"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Achat::class,
        ]);
    }
}
