<?php

namespace App\Form;

use App\Entity\Sortie;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('coup', TextType::class,[
                "label" => "Numéro"
            ])
            ->add('date_sortie', DateType::class,[
                "widget" => "single_text",
                'attr' => [
                    "label" => "Date de sorte"
                ]
            ])
            ->add('coup_sortie', IntegerType::class, [
                "label" => "Montant"
            ])
            ->add('cause', TextType::class, [
                "label" => "Motif"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,
        ]);
    }
}
