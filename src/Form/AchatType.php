<?php

namespace App\Form;

use App\Entity\Achat;
use App\Entity\Departement;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AchatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomAcheteur',TextType::class,[
                "label" => "Nom de l'acheteur"
            ])
            ->add('qteAchete',IntegerType::class,[
                "label" => "Quantité",
            ])
            ->add('dateEcheance',DateType::class,[
                "widget" => "single_text"
            ])
            ->add('avance',IntegerType::class)
            ->add('typePaiement',ChoiceType::class,[
                "choices" => [
                    "--Veuillez selectionner--" => "--Veuillez selectionner--",
                    "Mvola" => "Mvola",
                    "Orange money" => "Orange money",
                    "Airtel money" => "Airtel money",
                    "Cash" => "Cash",
                    "Virement bancaire" => "Virement bancaire",
                    "Abonné" => "Abonné"
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Achat::class,
        ]);
    }
}
