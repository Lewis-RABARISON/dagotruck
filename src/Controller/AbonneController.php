<?php

namespace App\Controller;

use App\Entity\Achat;
use App\Form\AbonneType;
use App\Repository\AchatRepository;
use App\Repository\DetailMaterielRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
* @Route("/abonne")
*/
class AbonneController extends AbstractController
{
    const TYPE_PAIEMENT = "Abonné";

    /**
     * @Route("/", name="abonne_index")
     */
    public function abonne(AchatRepository $achatRepository): Response
    {
        return $this->render('abonne/index.html.twig', [
            'abonnes' => $achatRepository->getAllAbonne()
        ]);
    }

    /**
     * @Route("/{code_facture}/nouveau-abonnement/", name="abonne_new", methods={"GET","POST"})
     */
    public function abonnement(Request $request,AchatRepository $achatRepository,DetailMaterielRepository $detailmaterielRepository,$code_facture): Response
    {
        $nomAbonnes = $achatRepository->nomAcheteur();

        $achat = new Achat();

        $form  = $this->createForm(AbonneType::class, $achat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data           = $request->request->all();
            $codeFacture    = $data["abonne"]["codeFacture"];
            $nomAcheteur    = $data['abonne_nomAcheteur'];
            $dateAchat      = new \DateTime();
            $dateEcheance   = new \DateTime($data["abonne"]["dateEcheance"]);
            $detailMateriel = $data["abonne"]["detailMateriel"];

            if (isset($data["abonne"]["detailMateriel"])) {
                foreach ($data["abonne"]["detailMateriel"] as $key => $materiel_id) {
                    $detailmateriel = $detailmaterielRepository->find($materiel_id);
                    $achat->addDetailMateriel($detailmateriel);

                    if (isset($data["qteAchete"][$key])) {
                        $achat->setQteAchete($data["qteAchete"][$key]);
                    }

                    if (isset($data["avance"][$key])) {
                        $achat->setAvance($data["avance"][$key]);
                    }

                    $achat->setCodeFacture($codeFacture);
                    $achat->setNomAcheteur($nomAcheteur);
                    $achat->setDateAchat($dateAchat);
                    $achat->setDateEcheance($dateEcheance);
                    $achat->setTypePaiement(self::TYPE_PAIEMENT);

                    $entityManager = $this->getDoctrine()->getManager();
        
                    $entityManager->persist($achat);
                    $this->addFlash('success','Abonné ajouté avec succès');
                    $entityManager->flush();
        
                    return $this->redirectToRoute('abonne_index');
                }
            }

        }

        return $this->render('abonne/new.html.twig', [
            'form' => $form->createView(),
            'nom_abonnes' => $nomAbonnes,
            'detail_abonnes' => $achatRepository->findBy(["codeFacture" => $code_facture])
        ]);
    }

    /**
     * @Route("/nouveau/abonnement/", name="abonnes_new", methods={"GET","POST"})
     */
    public function nouveauAbonnement(Request $request,EntityManagerInterface $manager,
                                        DetailMaterielRepository $detailmaterielRepository)
    {
        $data = $request->request->all();
        if (isset($data["materiel_id"])) {
            foreach ($data["materiel_id"] as $key => $materiel_id) {
                $achat = new Achat();
                $detailmateriel = $detailmaterielRepository->find($materiel_id);
                $achat->addDetailMateriel($detailmateriel);

                if (isset($data["quantity"][$key])) {
                    $achat->setQteAchete((int) $data["quantity"][$key]);
                }

                if (isset($data["avance"][$key])) {
                    $achat->setAvance((int) $data["avance"][$key]);
                }

                if (isset($data["nom_acheteur"][$key])) {
                    $achat->setNomAcheteur($data["nom_acheteur"][$key]);
                }

                if (isset($data["code_facture"][$key])) {
                    $achat->setCodeFacture($data["code_facture"][$key]);
                }

                if (isset($data["date_echeance"][$key])) {
                    $dateEcheance = new \Datetime($data["date_echeance"][$key]);
                    $achat->setDateEcheance($dateEcheance);
                }

                $dateAchat = new \Datetime();
                $tempsAchat = $dateAchat->format('h:i:s');
                $achat->setDateAchat($dateAchat);
                $achat->setTempsAchat($tempsAchat);

                $achat->setTypePaiement(self::TYPE_PAIEMENT);

                $manager->persist($achat);
                $manager->flush();
            }
        }

        return $this->json(["success" => true,"message" => "Abonné ajouté avec succès"]);
    }

    /**
    * @Route("/{code_facture}/detail", name="abonne_show")
    */
    public function show(AchatRepository $achatRepository,$code_facture)
    {
        return $this->render('abonne/show.html.twig', [
            'detail_abonnes' => $achatRepository->findBy(["codeFacture" => $code_facture])
        ]);
    }

    /**
     * @Route("/{code_facture}/delete", name="abonne_delete", methods={"DELETE"})
    */
    public function delete(AchatRepository $achatRepository,$code_facture)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $abonnesByCode = $achatRepository->findBy(["codeFacture" => $code_facture]);
        $abonnes = [];

        for ($i=0; $i < count($abonnesByCode); $i++) { 
            $abonne_id = $abonnesByCode[$i]->getId();
            array_push($abonnes,$achatRepository->find($abonne_id));
        }

        foreach ($abonnes as $abonne) {
            $entityManager->remove($abonne);
            $entityManager->flush();
        }
        
        $this->addFlash('success','Abonné supprimé avec succès');

        return $this->redirectToRoute('abonne_index');
    }
}
