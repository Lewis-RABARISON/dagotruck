<?php

namespace App\Controller;

use App\Entity\TypeMateriel;
use App\Form\TypeMaterielType;
use App\Repository\MaterielRepository;
use App\Repository\TypeMaterielRepository;
use App\Service\MessageFlashService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/type-materiel")
 */
class TypeMaterielController extends AbstractController
{
    private $manager;
    private $typeMaterielRepository;

    /**
     * Constructeur
     *
     * @param EntityManagerInterface $manager
     * @param TypeMaterielRepository $typeMaterielRepository
     */
    public function __construct(EntityManagerInterface $manager,
                                TypeMaterielRepository $typeMaterielRepository)
    {
        $this->manager = $manager;
        $this->typeMaterielRepository = $typeMaterielRepository;
    }
    
    /**
     * Liste des types de matériels
     * @Route("/", name="type_materiel_index", methods={"GET"})
     * @return Response
     */
    public function index(): Response
    {
        $type_materiels = $this->typeMaterielRepository->findAll();

        return $this->render('type_materiel/index.html.twig', compact("type_materiels"));
    }

    /**
     * Ajout type de matériel
     * @Route("/nouveau", name="type_materiel_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $typeMateriel = new TypeMateriel();
        $form = $this->createForm(TypeMaterielType::class, $typeMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($typeMateriel);
            $this->addFlash('success','Type de matériel ajouté avec succès');
            $this->manager->flush();

            return $this->redirectToRoute('type_materiel_index');
        }

        return $this->render('type_materiel/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Trouver matériel par type
     * @Route("/{marque_id}/marque", name="type_materiel_show")
     * @param int $marque_id
     * @param TypeMaterielRepository $typeMaterielRepository
     * @return Response
     */
    public function findMaterielByType($marque_id,TypeMaterielRepository $typeMaterielRepository): Response
    {
       $marques = $typeMaterielRepository->findMaterielByType($marque_id);
       
       return $this->render('type_materiel/show.html.twig',compact('marques'));
    }

    /**
     * Modifier type de matériel
     * @Route("/{id}/edit", name="type_materiel_edit", methods={"GET","POST"})
     * @param Request $request
     * @param TypeMateriel $typeMateriel
     * @return Response
     */
    public function edit(Request $request, TypeMateriel $typeMateriel): Response
    {
        $form = $this->createForm(TypeMaterielType::class, $typeMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success','Type de matériel modifié avec succès');
            $this->manager->flush();

            return $this->redirectToRoute('type_materiel_index');
        }

        return $this->render('type_materiel/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer type de matériel
     * @Route("/{id}/delete", name="type_materiel_delete", methods={"DELETE"})
     * @param TypeMateriel $typeMateriel
     * @param MaterielRepository $materielRepository
     * @return Response
     */
    public function delete(TypeMateriel $typeMateriel, MaterielRepository $materielRepository): Response
    {
        $type_materiel_id = $typeMateriel->getId();

        if ($type_materiel_id != NULL) {
            $materiel_by_id = $materielRepository->findBy(['typeMateriel' => $type_materiel_id]);
            $count_materiel = count($materiel_by_id);
            for ($i = 0; $i < $count_materiel; $i++){
                $materiel = $materielRepository->find($materiel_by_id[$i]->getId());
                $materiel->setTypeMateriel(NULL);

                $this->manager->persist($materiel);
                $this->manager->flush();
            }
        }

        $this->manager->remove($typeMateriel);
        $this->addFlash('success','Type de matériel supprimé avec succès');
        $this->manager->flush();

        return $this->redirectToRoute('type_materiel_index');
    }
}
