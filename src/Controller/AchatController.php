<?php

namespace App\Controller;

use App\Entity\Achat;
use App\Form\AchatType;
use App\Service\CartService;
use App\Repository\AchatRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
* @Route("/achat")
*/
class AchatController extends AbstractController
{
    /**
     * @Route("/", name="achat_index")
     */
    public function index(AchatRepository $achatRepository, Request $request): Response
    {
        $achats = $achatRepository->findAll();
        $dateAchats = $achatRepository->yearPurchase();
        $year = [];

        for ($i=0; $i < count($dateAchats); $i++) { 
            $date = new \Datetime($dateAchats[$i]["dateAchat"]);
           array_push($year, $date->format("Y"));
        }

        $years = array_unique($year);

        return $this->render('achat/index.html.twig', compact("achats","years"));
    }
   
    /**
     * @Route("/tri-achat-journalier", name="tri_achat_journalier")
    */
    public function SortingPurchaseByDay(Request $request,AchatRepository $achatRepository)
    {
        $data = $request->request->all();
        $data_achat_journalier = $data['data_achat_journalier'];

        if ($data_achat_journalier != "") {
            $purchaseByDays = $achatRepository->getPurchaseByDay($data_achat_journalier);

            $response = [
                'data' => $purchaseByDays,
                'success' => true,
            ];

            return $this->json($response);
        }
    }

    /**
     * @Route("/tri-achat-mensuel", name="tri_achat_mensuel")
    */
    public function SortingPurchaseByMonth(Request $request,AchatRepository $achatRepository)
    {
        $data = $request->request->all();
        $data_achat_mensuel = new \Datetime($data['data_achat_mensuel']);

        if ($data_achat_mensuel != "") {
            $purchaseByYearMonth = $achatRepository->getPurchaseByYearMonth($data_achat_mensuel);

            $response = [
                'data' => $purchaseByYearMonth,
                'success' => true,
            ];

            return $this->json($response);
        }
    }

    /**
     * @Route("/tri-achat-annuel", name="tri_achat_annuel")
    */
    public function SortingPurchaseByYears(Request $request,AchatRepository $achatRepository)
    {
        $data = $request->request->all();
        $data_achat_annuel = $data['data_achat_annuel'] ? $data['data_achat_annuel'] : "";

        if ($data_achat_annuel != "") {
            $purchaseByYears = $achatRepository->getPurchaseByYear($data_achat_annuel);

            $response = [
                'data' => $purchaseByYears,
                'success' => true,
            ];

            return $this->json($response);
        }
    }

    /**
     * @Route("/{code}/invoice", name="achat_invoice")
    */
    public function invoice(AchatRepository $achatRepository,$code): Response
    {
        $factures = $achatRepository->findBy(["codeFacture" => $code]);
        $codeFacture = $factures[0]->getCodeFacture();
        $nomAcheteur = $factures[0]->getNomAcheteur();
        $typePaiement = $factures[0]->getTypePaiement();
        $dateAchat = $factures[0]->getDateAchat();
        $tempsAchat = $factures[0]->getTempsAchat();

        $avance = [];

        if (count($factures)) {
            for ($i=0; $i < count($factures); $i++) { 
                array_push($avance, $factures[$i]->getAvance());
            }
        }

        $sum_avance = array_sum($avance);

        return $this->render('achat/invoice.html.twig', 
                            compact("factures","codeFacture","nomAcheteur",
                                    "typePaiement","dateAchat","tempsAchat", "sum_avance"));
    }

    /**
     * @Route("/proforma", name="achat_proforma")
    */
    public function proforma(CartService $cartService): Response
    {
        $total   = $cartService->getTotal();
        $paniers = $cartService->getFullCart();

        return $this->render('achat/proforma.html.twig',compact('paniers','total'));
    }

     /**
     * @Route("/{id}/edit", name="achat_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Achat $achat): Response
    {
        $form = $this->createForm(AchatType::class, $achat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $code = $achat->getCodeFacture();
            $achat->setCodeFacture($code);

            $this->addFlash('success','Achat modifié avec succès');
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('achat_index');
        }

        return $this->render('achat/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/delete", name="achat_delete", methods={"DELETE"})
    */
    public function delete(Achat $achat): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($achat);
        $this->addFlash('success','Achat supprimé avec succès');
        $entityManager->flush();

        return $this->redirectToRoute('achat_index');
    }
}
