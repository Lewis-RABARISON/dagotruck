<?php

namespace App\Controller;

use App\Entity\DetailMateriel;
use App\Form\DetailMaterielType;
use App\Repository\DepartementRepository;
use App\Repository\DetailMaterielRepository;
use App\Repository\MaterielRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/detail-materiel")
 */
class DetailMaterielController extends AbstractController
{
    /**
     * @Route("/", name="detail_materiel_index", methods={"GET"})
     */
    public function index(DetailMaterielRepository $detailMaterielRepository, Request $request,DepartementRepository $departementRepository): Response
    {
        $limit = 10;
        $page = (int)$request->query->get('page',1);
        $material_detail_research_value = $request->query->get("q");
        $detail_materiels = $detailMaterielRepository->getPaginatedMaterialDetail($page,$limit,$material_detail_research_value);
        $total_detail_materiel = $detailMaterielRepository->getTotalMaterialDetail();

        if ($request->isXmlHttpRequest()){
            $response = [
                "success" => true,
                "content" => $this->renderView("detail_materiel/_detail_materiels.html.twig",['detail_materiels' => $detail_materiels])
            ];

            return $this->json($response);
        }

        return $this->render('detail_materiel/index.html.twig', compact('limit','page','detail_materiels','total_detail_materiel'));
    }

    /**
     * @Route("/nouveau", name="detail_materiel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $detailMateriel = new DetailMateriel();
        $form = $this->createForm(DetailMaterielType::class, $detailMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $dateApprovisionnement = new \DateTime();
            $detailMateriel->setDateApprovisionnement($dateApprovisionnement);

            if ($detailMateriel->getQte() < 0 ) {
                $this->addFlash('danger','La quantité ne doit pas être inférieur à 0');
                return $this->redirectToRoute('detail_materiel_new');
            }

            if ( $detailMateriel->getPrixUnitaire() < 0) {
                $this->addFlash('danger','Le prix unitaire ne doit pas être inférieur à 0');
                return $this->redirectToRoute('detail_materiel_new');
            }

            $entityManager->persist($detailMateriel);
            $this->addFlash('success','Détail de matériel ajouté avec succès');
            $entityManager->flush();

            return $this->redirectToRoute('detail_materiel_index');
        }

        return $this->render('detail_materiel/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/edit", name="detail_materiel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DetailMateriel $detailMateriel,
                         DetailMaterielRepository $detailMaterielRepository,
                         MaterielRepository $materielRepository, DepartementRepository $departementRepository): Response
    {
        $form = $this->createForm(DetailMaterielType::class, $detailMateriel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success','Détail de matériel modifié avec succès');
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('detail_materiel_index');
        }

        return $this->render('detail_materiel/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * find material by type
     *
     * @param int $type_materiel_id
     * @param MaterielRepository $materielRepository
     * @Route("/{type_materiel_id}/materiel", name="materiel_type")
     */
    public function materielByType($type_materiel_id,MaterielRepository $materielRepository)
    {
        $type_materiel_id = $materielRepository->materielByType($type_materiel_id);

        return $this->json(["success" => true,"type_materiel_id" => $type_materiel_id]);
    }

    /**
     * Research material detail
     * @Route("/recherche", name="recherche_detail_materiel")
     */
    public function researchDetailMateriel(Request $request, DetailMaterielRepository $detailMaterielRepository)
    {
        $value_to_research = $request->query->get('q');
        $detail_materiels = $detailMaterielRepository->findAllMaterialsDetail();

        $response = [
            "success" => true,
            "value_to_research" => $this->json($detail_materiels, 200, [], ['groups' => 'detail-materiel:read'])
        ];
        
        if ($value_to_research != "") {
            $materialDetailFiltered = $detailMaterielRepository->researchMaterialDetail($value_to_research);

            $response = [
                "success" => true,
                "value_to_research" => $this->json($materialDetailFiltered, 200, [], ['groups' => 'detail-materiel:read'])
            ];
        }
        
        return $this->json($response);
    }

    /**
     * @Route("/{id}/delete", name="detail_materiel_delete", methods={"DELETE"})
     */
    public function delete(DetailMateriel $detailMateriel): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $entityManager->remove($detailMateriel);
        $this->addFlash('success','Détail de matériel supprimé avec succès');
        $entityManager->flush();

        return $this->redirectToRoute('detail_materiel_index');
    }
}
