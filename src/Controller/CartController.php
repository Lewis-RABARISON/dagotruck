<?php

namespace App\Controller;

use App\Entity\Achat;
use App\Entity\DetailMateriel;
use App\Repository\DepartementRepository;
use App\Repository\DetailMaterielRepository;
use App\Repository\MaterielRepository;
use App\Service\CartService;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
* @Route("/panier")
*/
class CartController extends AbstractController
{
    private $cartService;
    private $detailmaterielRepository;
    private $manager;
    private $materiel_repository;
    private $departement_repository;
    private $session;

    public function __construct(CartService $cartService,
                                DetailMaterielRepository $detailmaterielRepository,
                                EntityManagerInterface $manager,
                                PaginatorInterface $paginator, MaterielRepository $materiel_repository,
                                DepartementRepository $departement_repository, SessionInterface $session)
    {
        $this->cartService = $cartService;
        $this->detailmaterielRepository = $detailmaterielRepository;
        $this->manager = $manager;
        $this->paginator = $paginator;
        $this->materiel_repository = $materiel_repository;
        $this->departement_repository = $departement_repository;
        $this->session = $session;
    }
    
    /**
     * Liste des achats dans le panier
     * @Route("/", name="cart_index")
     */
    public function index(Request $request): Response
    {
        $panierWithData = $this->cartService->getFullCart();

        /*$panierWithData  = $this->paginator->paginate(
            $this->cartService->getFullCart(),
            $request->query->getInt('page',1),
            6
        );*/
        $total = $this->cartService->getTotal();

        return $this->render('cart/index.html.twig', [
            'items' => $panierWithData,
            'total' => $total,
        ]);
    }

    /**
     * Validation de l'achat
     * @Route("/valid", name="cart_valid_purchase")
     */
    public function validPurchase(Request $request, SessionInterface $session)
    {
        $data = $request->request->all();

        if (isset($data["materiel_id"])) {
            foreach ($data["materiel_id"] as $key => $materiel_id) {
                /*  Achat */
                $achat = new Achat();
                $detail_materiel = $this->detailmaterielRepository->find($materiel_id);
                $achat->addDetailMateriel($detail_materiel);
//                $qte_detail_materiel = $detail_materiel->getQte();

                $qte_achete = $data["quantity"][$key];
                $nom_acheteur = $data["nom"][$key];
                $type_paiement = $data["type_paiement"][$key];
                $avance = $data["avance"][$key];
                $date_echeance = $data["date_echeance"][$key];

                /*if (isset($qte_achete) && !empty($qte_detail_materiel) && ($qte_detail_materiel > $qte_achete)) {
                    $achat->setQteAchete($qte_achete);
                }*/

                if (isset($qte_achete)) {
                    $achat->setQteAchete($qte_achete);
                }

                if (isset($nom_acheteur)) {
                    $achat->setNomAcheteur($nom_acheteur);
                }

                if (isset($type_paiement)) {
                    $achat->setTypePaiement($type_paiement);
                }

                if (isset($avance)) {
                    $achat->setAvance((int) $avance);
                }

                if (isset($date_echeance)) {
                    if (isset($type_paiement) && $type_paiement !== "Abonné") {
                        $achat->setDateEcheance(null);
                    } 
                    else if (isset($type_paiement) && $type_paiement === "Abonné"){
                        $dateEcheance = new \Datetime($date_echeance);
                        $achat->setDateEcheance($dateEcheance);
                    }
                }

                $dateAchat  = new \Datetime();
                $tempsAchat = $dateAchat->format('h:i:s');

                $achat->setDateAchat($dateAchat);
                $achat->setTempsAchat($tempsAchat);

                $code = substr(md5($dateAchat->format('Y-m-d h:i')),0,6);
                $achat->setCodeFacture($code);

                /* Détail matériel */
                /*$qte = $qte_detail_materiel - $qte_achete == 0 ? 0 : $qte_detail_materiel - $qte_achete;

                $this->updateQtyMaterialDetail($detail_materiel, $qte);*/

                $this->manager->persist($achat);
//                $this->manager->persist($detail_materiel);
                $this->manager->flush();
                $this->removeAllPurchase($session);
            }
        }

        return $this->json(["success" => true,"message" => "L'achat est ajouté avec succès"]);
    }

//    /**
//     * Mettre a jour la quantite apres l'achat
//     * @param $detail_materiel
//     * @param $qte
//     */
//    public function updateQtyMaterialDetail($detail_materiel, $qte)
//    {
//        $nom_detail_materiel       = $detail_materiel->getNomMateriel();
//        $categorie_detail_materiel = $detail_materiel->getCategorie();
//        $prix_detail_materiel      = $detail_materiel->getPrixUnitaire();
//        $date_detail_materiel      = $detail_materiel->getDateApprovisionnement();
//        $reference_detail_materiel = $detail_materiel->getReference();
//
//        $material_id = $detail_materiel->getMateriel()->getId();
//        $materiel    = $this->materiel_repository->find($material_id);
//
//        $departement_id = $detail_materiel->getDepartement()->getId();
//        $departement    = $this->departement_repository->find($departement_id);
//
//        $detail_materiel->setQte($qte);
//        $detail_materiel->setNomMateriel($nom_detail_materiel);
//        $detail_materiel->setCategorie($categorie_detail_materiel);
//        $detail_materiel->setPrixUnitaire($prix_detail_materiel);
//        $detail_materiel->setDateApprovisionnement($date_detail_materiel);
//        $detail_materiel->setReference($reference_detail_materiel);
//        $detail_materiel->setMateriel($materiel);
//        $detail_materiel->setDepartement($departement);
//    }

    /**
     * Suppression de toute de l'achat dans le panier
     * @Route("/remove-purchase", name="remove_all_purchase")
     */
    public function removeAllPurchase(SessionInterface $session)
    {
        $session->remove("panier");

        return $this->json(["success" => true,"message" => "Tous les achas sont supprimés avec succès"]);
    }

    /**
     * Ajoute le matériel à acheter dans le panier
     * @Route("/add/{id}", name="cart_add")
     */
    public function add($id, SessionInterface $session)
    {
        $panier = $session->get('panier',[]);

        $array_cart_detail_materiel_id = [];
        $detail_materiel = $this->detailmaterielRepository->find($id);
         $qte_detail_materiel = $detail_materiel->getTotalQty();

        if (count($panier) > 0){
            foreach ($panier as $id_detail_materiel => $quantity) {
                array_push($array_cart_detail_materiel_id, $id_detail_materiel);
            }
        }

        if (in_array($id, $array_cart_detail_materiel_id)){
            $qty_achete = $panier[$id];
            if ($qty_achete >= $qte_detail_materiel){
                $response = [
                    "success" => true,
                    "message" => "La quantité à acheter dépasse déja la quantité disponible"
                ];

                return $this->json($response);
            }

            $this->cartService->add($id);
            $response = [
                "success" => true,
                "message" => "Le détail matériel est ajouté dans le panier"
            ];

            return $this->json($response);
        }

        $this->cartService->add($id);
        $response = [
            "success" => true,
            "message" => "Le détail matériel est ajouté dans le panier"
        ];

        return $this->json($response);
    }

    /**
     * Augmente la quantité du matériel à acheter
     * @Route("/increase/{id}", name="cart_increase")
     */
    public function increase($id)
    {
        $panier = $this->session->get('panier',[]);

        $array_cart_detail_materiel_id = [];
        $detail_materiel = $this->detailmaterielRepository->find($id);
        $qte_detail_materiel = $detail_materiel->getTotalQty();

        if (count($panier) > 0){
            foreach ($panier as $id_detail_materiel => $quantity) {
                array_push($array_cart_detail_materiel_id, $id_detail_materiel);
            }
        }

        if (in_array($id, $array_cart_detail_materiel_id)){
            $qty_achete = $panier[$id];

            if ($qty_achete >= $qte_detail_materiel){
                $this->addFlash('success','La quantité à acheter dépasse déja la quantité disponible');

                return $this->redirectToRoute("cart_index");
            }

            $this->cartService->add($id);

            return $this->redirectToRoute("cart_index");
        }

        $this->cartService->add($id);
        
        return $this->redirectToRoute("cart_index");
    }

    /**
     * Dimunie la quantité du matériel à acheter
     * @Route("/decrease/{id}", name="cart_decrease")
     */
    public function decrease($id)
    {
        $this->cartService->decreaseQuantity($id);
        
        return $this->redirectToRoute("cart_index");
    }

    /**
     * Supprime le panier
     * @Route("/remove/{id}", name="cart_remove")
     */
    public function remove($id)
    {
        $this->cartService->remove($id);

        return $this->redirectToRoute("cart_index");
    }
}
