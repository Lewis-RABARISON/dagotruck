<?php

namespace App\Controller;

use App\Entity\Departement;
use App\Form\DepartementType;
use App\Repository\DepartementRepository;
use App\Repository\DetailMaterielRepository;
use App\Service\MessageFlashService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/departement")
 */
class DepartementController extends AbstractController
{
    private $manager;
    private $message;

    /**
     * Constructeur
     *
     * @param EntityManagerInterface $manager
     * @param MessageFlashService $message
     */
    public function __construct(EntityManagerInterface $manager,MessageFlashService $message)
    {
        $this->manager = $manager;
        $this->message = $message;
    }

    /**
     * Liste des départements
     * @Route("/", name="departement_index", methods={"GET"})
     */
    public function index(DepartementRepository $departementRepository): Response
    {
        return $this->render('departement/index.html.twig', [
            'departements' => $departementRepository->findAll(),
        ]);
    }

    /**
     * Ajouter département
     * @Route("/nouveau", name="departement_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $departement = new Departement();
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($departement);
            $this->message->messageFlash("Département","ajouté");
            $this->manager->flush();

            return $this->redirectToRoute('departement_index');
        }

        return $this->render('departement/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Modifier département
     * @Route("/{id}/edit", name="departement_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Departement $departement): Response
    {
        $form = $this->createForm(DepartementType::class, $departement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->message->messageFlash("Département","modifié");
            $this->manager->flush();

            return $this->redirectToRoute('departement_index');
        }

        return $this->render('departement/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer département
     * @Route("/{id}/delete", name="departement_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Departement $departement,
                           DetailMaterielRepository $detailMaterielRepository): Response
    {
         $array_detail_materiels = $departement->getDetailMateriels()->getValues();
         $count_array_detail_materiels = count($array_detail_materiels);

         if ($count_array_detail_materiels > 0) {
             for ($i = 0; $i < $count_array_detail_materiels; $i++){
                 $detail_materiel = $detailMaterielRepository->find($array_detail_materiels[$i]->getId());
                 $detail_materiel->setDepartement(NULL);
                 $this->manager->persist($departement);
                 $this->manager->flush();
             }
         }

        $this->manager->remove($departement);
        $this->message->messageFlash("Département","supprimé");
        $this->manager->flush();

        return $this->redirectToRoute('departement_index');
    }
}
