<?php

namespace App\Controller;

use App\Entity\Materiel;
use App\Form\MaterielType;
use App\Repository\DetailMaterielRepository;
use App\Repository\MaterielRepository;
use App\Service\MessageFlashService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/materiel")
 */
class MaterielController extends AbstractController
{
    /**
     * Constructeur
     *
     * @param EntityManagerInterface $manager
     * @param MessageFlashService $message
     */
    public function __construct(EntityManagerInterface $manager,MessageFlashService $message)
    {
        $this->manager = $manager;
        $this->message = $message;
    }

    /**
     * Liste des matériels
     * @Route("/", name="materiel_index", methods={"GET"})
     */
    public function index(MaterielRepository $materielRepository): Response
    {
        return $this->render('materiel/index.html.twig', [
            'materiels' => $materielRepository->findAll()
        ]);
    }

    /**
     * Ajouter matériel
     * @Route("/nouveau", name="materiel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $materiel = new Materiel();
        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->manager->persist($materiel);
            $this->message->messageFlash("Matériel","ajouté");
            $this->manager->flush();

            return $this->redirectToRoute('materiel_index');
        }

        return $this->render('materiel/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Modifier matériel
     * @Route("/{id}/edit", name="materiel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Materiel $materiel): Response
    {
        $form = $this->createForm(MaterielType::class, $materiel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->message->messageFlash("Matériel","modifié");
            $this->manager->flush();

            return $this->redirectToRoute('materiel_index');
        }

        return $this->render('materiel/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Supprimer matériel
     * @Route("/{id}/delete", name="materiel_delete", methods={"DELETE"})
     */
    public function delete(Materiel $materiel,
                           DetailMaterielRepository $detailMaterielRepository): Response
    {
        $array_materiels = $materiel->getDetailMateriels()->getValues();
        $count_array_materiels = count($array_materiels);

        if ($count_array_materiels > 0){
            for ($i=0; $i < $count_array_materiels; $i++ ){
                $detail_materiel = $detailMaterielRepository->find($array_materiels[$i]->getId());
                $detail_materiel->setMateriel(NULL);
                $this->manager->persist($detail_materiel);
                $this->manager->flush();
            }
        }

        $this->manager->remove($materiel);
        $this->message->messageFlash("Matériel","supprimé");
        $this->manager->flush();

        return $this->redirectToRoute('materiel_index');
    }
}
