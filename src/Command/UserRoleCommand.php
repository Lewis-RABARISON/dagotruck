<?php

namespace App\Command;

use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UserRoleCommand extends Command
{
    protected static $defaultName = 'app:user-role';

    private $userRepository;
    private $em;

    function __construct(UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->userRepository = $userRepository;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription("Role de l'utilisateur")
            ->addArgument('email', InputArgument::REQUIRED, "Email de l'utilisateur")
            ->addArgument('roles', InputArgument::REQUIRED, "Role a attribué à l'utilisateur")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $roles = $input->getArgument('roles');
        $user = $this->userRepository->findOneBy(["email" => $email]);
        $array_role = [];
        $array_role[] = $roles;

        if ($user) {
            $user->setRoles($array_role);
            $this->em->flush();
            $io->success($roles.' est attribué à '.$email.' avec succès');
        }
        else {
            $io->error("Erreur lors de l'attribution de la role ".$roles." à ".$email);
        }

        return 0;
    }
}
