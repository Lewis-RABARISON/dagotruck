<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\SortieRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=SortieRepository::class)
 * @UniqueEntity(fields={"coup"},message="Ce numéro existe déjà")
 */
class Sortie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Le numéro est obligatoire")
     * @Assert\Length(max=20,maxMessage="La caractère du numéro est au maximum 20 caractères")
     */
    private $coup;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank(message="La date de sortie est obligatoire")
     * @Assert\Date
     * @var string A "Y-m-d" formatted value
     */
    private $date_sortie;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank(message="Le montant est obligatoire")
     * @Assert\PositiveOrZero(message="Le montant est au minimum 0")
     */
    private $coup_sortie;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message="Le motif est obligatoire")
     */
    private $cause;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoup(): ?string
    {
        return $this->coup;
    }

    public function setCoup(string $coup): self
    {
        $this->coup = $coup;

        return $this;
    }

    public function getDateSortie()
    {
        return $this->date_sortie;
    }

    public function setDateSortie($date_sortie): self
    {
        $this->date_sortie = $date_sortie;

        return $this;
    }

    public function getCoupSortie(): ?int
    {
        return $this->coup_sortie;
    }

    public function setCoupSortie(int $coup_sortie): self
    {
        $this->coup_sortie = $coup_sortie;

        return $this;
    }

    public function getCause(): ?string
    {
        return $this->cause;
    }

    public function setCause(string $cause): self
    {
        $this->cause = $cause;

        return $this;
    }
}
