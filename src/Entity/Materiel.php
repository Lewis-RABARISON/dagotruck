<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\MaterielRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=MaterielRepository::class)
 */
class Materiel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Le nom du fournisseur est obligatoire")
     * @Assert\Length(max=255,maxMessage="La caractère du nom du fournisseur est au maximum 255 caractères")
     */
    private $nomFournisseur;

    /**
     * @ORM\Column(type="string", length=65)
     * @Assert\NotBlank(message="Le nom ou marque est obligatoire")
     * @Assert\Length(max=65,maxMessage="La caractère du nom ou marque est au maximum 65 caractères")
     */
    private $nomMarque;

    /**
     * @ORM\ManyToOne(targetEntity=TypeMateriel::class, inversedBy="materiels")
     * @ORM\JoinColumn(nullable=true)
     */
    private $typeMateriel;

    /**
     * @ORM\OneToMany(targetEntity=DetailMateriel::class, mappedBy="materiel")
     */
    private $detailMateriels;

    public function __construct()
    {
        $this->detailMateriels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomFournisseur(): ?string
    {
        return $this->nomFournisseur;
    }

    public function setNomFournisseur(string $nomFournisseur): self
    {
        $this->nomFournisseur = $nomFournisseur;

        return $this;
    }

    public function getNomMarque(): ?string
    {
        return $this->nomMarque;
    }

    public function setNomMarque(string $nomMarque): self
    {
        $this->nomMarque = $nomMarque;

        return $this;
    }

    public function getTypeMateriel(): ?TypeMateriel
    {
        return $this->typeMateriel;
    }

    public function setTypeMateriel(?TypeMateriel $typeMateriel): self
    {
        $this->typeMateriel = $typeMateriel;

        return $this;
    }

    /**
     * @return Collection|DetailMateriel[]
     */
    public function getDetailMateriels(): Collection
    {
        return $this->detailMateriels;
    }

    public function addDetailMateriel(DetailMateriel $detailMateriel): self
    {
        if (!$this->detailMateriels->contains($detailMateriel)) {
            $this->detailMateriels[] = $detailMateriel;
            $detailMateriel->setMateriel($this);
        }

        return $this;
    }

    public function removeDetailMateriel(DetailMateriel $detailMateriel): self
    {
        if ($this->detailMateriels->removeElement($detailMateriel)) {
            // set the owning side to null (unless already changed)
            if ($detailMateriel->getMateriel() === $this) {
                $detailMateriel->setMateriel(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nomMarque;
    }
}
