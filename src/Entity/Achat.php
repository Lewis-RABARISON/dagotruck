<?php

namespace App\Entity;

use App\Repository\AchatRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AchatRepository::class)
 */
class Achat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomAcheteur;

    /**
     * @ORM\ManyToMany(targetEntity=DetailMateriel::class, inversedBy="achats")
     */
    private $detailMateriel;

    /**
     * @ORM\Column(type="integer")
     */
    private $qteAchete;

    /**
     * @ORM\Column(type="date")
     */
    private $dateAchat;

    /**
     * @ORM\Column(type="string", length=70)
     */
    private $typePaiement;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateEcheance;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $avance;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $codeFacture;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $tempsAchat;

    public function __construct()
    {
        $this->detailMateriel = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomAcheteur(): ?string
    {
        return $this->nomAcheteur;
    }

    public function setNomAcheteur(string $nomAcheteur): self
    {
        $this->nomAcheteur = $nomAcheteur;

        return $this;
    }

    /**
     * @return Collection|DetailMateriel[]
     */
    public function getDetailMateriel(): Collection
    {
        return $this->detailMateriel;
    }

    public function addDetailMateriel(DetailMateriel $detailMateriel): self
    {
        if (!$this->detailMateriel->contains($detailMateriel)) {
            $this->detailMateriel[] = $detailMateriel;
        }

        return $this;
    }

    public function removeDetailMateriel(DetailMateriel $detailMateriel): self
    {
        $this->detailMateriel->removeElement($detailMateriel);

        return $this;
    }

    public function getQteAchete(): ?int
    {
        return $this->qteAchete;
    }

    public function setQteAchete(int $qteAchete): self
    {
        $this->qteAchete = $qteAchete;

        return $this;
    }

    public function getDateAchat(): ?\DateTimeInterface
    {
        return $this->dateAchat;
    }

    public function setDateAchat(?\DateTimeInterface $dateAchat): self
    {
        $this->dateAchat = $dateAchat;

        return $this;
    }

    public function getTypePaiement(): ?string
    {
        return $this->typePaiement;
    }

    public function setTypePaiement(string $typePaiement): self
    {
        $this->typePaiement = $typePaiement;

        return $this;
    }

    public function getDateEcheance(): ?\DateTimeInterface
    {
        return $this->dateEcheance;
    }

    public function setDateEcheance(?\DateTimeInterface $dateEcheance): self
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    public function getAvance(): ?int
    {
        return $this->avance;
    }

    public function setAvance(?int $avance): self
    {
        $this->avance = $avance;

        return $this;
    }

    public function getCodeFacture(): ?string
    {
        return $this->codeFacture;
    }

    public function setCodeFacture(string $codeFacture): self
    {
        $this->codeFacture = $codeFacture;

        return $this;
    }

    public function getTempsAchat(): ?string
    {
        return $this->tempsAchat;
    }

    public function setTempsAchat(string $tempsAchat)
    {
        $this->tempsAchat = $tempsAchat;

        return $this;
    }
}
