<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use App\Repository\DetailMaterielRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DetailMaterielRepository::class)
 * @UniqueEntity(fields={"nom_materiel"},message="Ce détail matériel existe déjà")
 * @UniqueEntity(fields={"reference"},message="Ce reference existe déjà")
 */
class DetailMateriel
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("detail-materiel:read")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Materiel::class, inversedBy="detailMateriels")
     * @ORM\JoinColumn(nullable=true)
     * @Assert\NotBlank(message="Le nom du matériel est obligatoire")
     */
    private $materiel;

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(message="Le nom du matériel est obligatoire")
     * @Assert\Length(max=100,maxMessage="La caractère du nom du matériel est au maximum 100 caractères")
     * @Groups("detail-materiel:read")
     */
    private $nom_materiel;

    /**
     * @ORM\Column(type="string", length=10)
     * @Assert\Length(max=10,maxMessage="La caractère du categorie est au maximum 10 caractères")
     * @Groups("detail-materiel:read")
     */
    private $categorie;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Assert\NotBlank(message="La quantité est obligatoire")
     * @Assert\PositiveOrZero(message="La quantité minimal est 0")
     * @Groups("detail-materiel:read")
     */
    private $qte;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank(message="Le prix unitaire est obligatoire")
     * @Assert\PositiveOrZero(message="Le prix minimal est 0")
     * @Groups("detail-materiel:read")
     */
    private $prixUnitaire;

    /**
     * @ORM\ManyToOne(targetEntity=Departement::class, inversedBy="detailMateriels")
     * @ORM\JoinColumn(nullable=true)
     */
    private $departement;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("detail-materiel:read")
     */
    private $dateApprovisionnement;

    /**
     * @ORM\ManyToMany(targetEntity=Achat::class, mappedBy="detailMateriel")
     */
    private $achats;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\NotBlank(message="Le réference est obligatoire")
     * @Groups("detail-materiel:read")
     */
    private $reference;

    public function __construct()
    {
        $this->achats = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMateriel(): ?Materiel
    {
        return $this->materiel;
    }

    public function setMateriel(?Materiel $materiel): self
    {
        $this->materiel = $materiel;

        return $this;
    }

    public function getNomMateriel(): ?string
    {
        return $this->nom_materiel;
    }

    public function setNomMateriel(string $nom_materiel): self
    {
        $this->nom_materiel = $nom_materiel;

        return $this;
    }

    public function getCategorie(): ?string
    {
        return $this->categorie;
    }

    public function setCategorie(string $categorie): self
    {
        $this->categorie = $categorie;

        return $this;
    }

    public function getQte(): ?int
    {
        return $this->qte;
    }

    public function setQte(int $qte): self
    {
        $this->qte = $qte;

        return $this;
    }

    public function getPrixUnitaire(): ?float
    {
        return $this->prixUnitaire;
    }

    public function setPrixUnitaire(float $prixUnitaire): self
    {
        $this->prixUnitaire = $prixUnitaire;

        return $this;
    }

    public function getDepartement(): ?Departement
    {
        return $this->departement;
    }

    public function setDepartement(?Departement $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getDateApprovisionnement(): ?\DateTimeInterface
    {
        return $this->dateApprovisionnement;
    }

    public function setDateApprovisionnement(\DateTimeInterface $dateApprovisionnement): self
    {
        $this->dateApprovisionnement = $dateApprovisionnement;

        return $this;
    }

    /**
     * @return Collection|Achat[]
     */
    public function getAchats(): Collection
    {
        return $this->achats;
    }

    public function addAchat(Achat $achat): self
    {
        if (!$this->achats->contains($achat)) {
            $this->achats[] = $achat;
            $achat->addDetailMateriel($this);
        }

        return $this;
    }

    public function removeAchat(Achat $achat): self
    {
        if ($this->achats->removeElement($achat)) {
            $achat->removeDetailMateriel($this);
        }

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getTotalQty()
    {
        $qty_initial = $this->getQte() != 0 ? $this->getQte() : 0;
        $achats = $this->getAchats()->getValues();
        $qte_achete = [];
        $total_qte_achete = 0;

        if (count($achats) > 0){
            for ($i=0; $i < count($achats); $i++){
                array_push($qte_achete,$achats[$i]->getQteAchete());
            }
            $total_qte_achete = array_sum($qte_achete);
        }

        $qteTotal = $qty_initial - $total_qte_achete;

        return $qteTotal;
    }
}
