<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\DepartementRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=DepartementRepository::class)
 * @UniqueEntity(fields={"nomDepartement"},message="Ce département existe déjà")
 * @UniqueEntity(fields={"contact"},message="Ce contact existe déjà")
 */
class Departement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("detail-materiel:read")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=60)
     * @Assert\NotBlank(message="Le nom du département est obligatoire")
     * @Assert\Length(max=60,maxMessage="La caractère du nom de département est au maximum 60 caractères")
     * @Groups("detail-materiel:read")
     */
    private $nomDepartement;

    /**
     * @ORM\Column(type="string", length=90)
     * @Assert\NotBlank(message="Le lieu est obligatoire")
     * @Assert\Length(max=90,maxMessage="La caractère du lieu est au maximum 90 caractères")
     * @Groups("detail-materiel:read")
     */
    private $lieu;

    /**
     * @ORM\Column(type="string", length=15)
     * @Assert\NotBlank(message="Le contact est obligatoire")
     * @Assert\Length(min=10,max=15,maxMessage="La caractère du contact est au maximum 15 caractères",minMessage="La caractère du contact est au minimum 10 caractères")
     * @Groups("detail-materiel:read")
     */
    private $contact;

    /**
     * @ORM\OneToMany(targetEntity=DetailMateriel::class, mappedBy="departement")
     */
    private $detailMateriels;

    public function __construct()
    {
        $this->detailMateriels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomDepartement(): ?string
    {
        return $this->nomDepartement;
    }

    public function setNomDepartement(string $nomDepartement): self
    {
        $this->nomDepartement = $nomDepartement;

        return $this;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    public function getContact(): ?string
    {
        return $this->contact;
    }

    public function setContact(string $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return Collection|DetailMateriel[]
     */
    public function getDetailMateriels(): Collection
    {
        return $this->detailMateriels;
    }

    public function addDetailMateriel(DetailMateriel $detailMateriel): self
    {
        if (!$this->detailMateriels->contains($detailMateriel)) {
            $this->detailMateriels[] = $detailMateriel;
            $detailMateriel->setDepartement($this);
        }

        return $this;
    }

    public function removeDetailMateriel(DetailMateriel $detailMateriel): self
    {
        if ($this->detailMateriels->removeElement($detailMateriel)) {
            // set the owning side to null (unless already changed)
            if ($detailMateriel->getDepartement() === $this) {
                $detailMateriel->setDepartement(null);
            }
        }

        return $this;
    }
}
