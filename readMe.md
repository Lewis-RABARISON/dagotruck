Il y a 3 rôle
- ROLE_USER
- ROLE_RESPONSABLE
- ROLE_ADMIN

## ROLE_USER
- Voir type de matériel
- Voir matériel
- Voir département
- Voir détail matériel
- Faire des achats
- Voir ces achats

## ROLE_RESPONSABLE
- Gérer type de matériel
- Gérer matériel
- Gérer département
- Gérer détail matériel
- Gérer panier 
- Gérer les achats
- Gérer les abonnés
- Trier achat journalier,mensuel,annuel

## ROLE_ADMIN
- Gérer utilisateur

### Il y a une hiérarchie de rôle
- ROLE ADMIN a l'accès ROLE RESPONSABLE




