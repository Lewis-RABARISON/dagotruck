<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210820185417 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE achat (id INT AUTO_INCREMENT NOT NULL, nom_acheteur VARCHAR(255) NOT NULL, qte_achete INT NOT NULL, date_achat DATE NOT NULL, type_paiement VARCHAR(70) NOT NULL, date_echeance DATETIME DEFAULT NULL, avance INT DEFAULT NULL, code_facture VARCHAR(10) NOT NULL, temps_achat VARCHAR(8) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE achat_detail_materiel (achat_id INT NOT NULL, detail_materiel_id INT NOT NULL, INDEX IDX_3F3E76ACFE95D117 (achat_id), INDEX IDX_3F3E76AC798367FE (detail_materiel_id), PRIMARY KEY(achat_id, detail_materiel_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE departement (id INT AUTO_INCREMENT NOT NULL, nom_departement VARCHAR(60) NOT NULL, lieu VARCHAR(90) NOT NULL, contact VARCHAR(15) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE detail_materiel (id INT AUTO_INCREMENT NOT NULL, materiel_id INT NOT NULL, departement_id INT NOT NULL, nom_materiel VARCHAR(100) NOT NULL, categorie VARCHAR(10) NOT NULL, qte INT DEFAULT NULL, prix_unitaire DOUBLE PRECISION NOT NULL, date_approvisionnement DATETIME NOT NULL, reference VARCHAR(20) NOT NULL, INDEX IDX_EE0C594A16880AAF (materiel_id), INDEX IDX_EE0C594ACCF9E01E (departement_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE materiel (id INT AUTO_INCREMENT NOT NULL, type_materiel_id INT NOT NULL, nom_fournisseur VARCHAR(255) NOT NULL, nom_marque VARCHAR(65) NOT NULL, INDEX IDX_18D2B0915D91DD3E (type_materiel_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE qty_updated (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sortie (id INT AUTO_INCREMENT NOT NULL, coup VARCHAR(20) NOT NULL, date_sortie DATE NOT NULL, coup_sortie INT NOT NULL, cause LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_materiel (id INT AUTO_INCREMENT NOT NULL, description_type VARCHAR(80) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE achat_detail_materiel ADD CONSTRAINT FK_3F3E76ACFE95D117 FOREIGN KEY (achat_id) REFERENCES achat (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE achat_detail_materiel ADD CONSTRAINT FK_3F3E76AC798367FE FOREIGN KEY (detail_materiel_id) REFERENCES detail_materiel (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE detail_materiel ADD CONSTRAINT FK_EE0C594A16880AAF FOREIGN KEY (materiel_id) REFERENCES materiel (id)');
        $this->addSql('ALTER TABLE detail_materiel ADD CONSTRAINT FK_EE0C594ACCF9E01E FOREIGN KEY (departement_id) REFERENCES departement (id)');
        $this->addSql('ALTER TABLE materiel ADD CONSTRAINT FK_18D2B0915D91DD3E FOREIGN KEY (type_materiel_id) REFERENCES type_materiel (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE achat_detail_materiel DROP FOREIGN KEY FK_3F3E76ACFE95D117');
        $this->addSql('ALTER TABLE detail_materiel DROP FOREIGN KEY FK_EE0C594ACCF9E01E');
        $this->addSql('ALTER TABLE achat_detail_materiel DROP FOREIGN KEY FK_3F3E76AC798367FE');
        $this->addSql('ALTER TABLE detail_materiel DROP FOREIGN KEY FK_EE0C594A16880AAF');
        $this->addSql('ALTER TABLE materiel DROP FOREIGN KEY FK_18D2B0915D91DD3E');
        $this->addSql('DROP TABLE achat');
        $this->addSql('DROP TABLE achat_detail_materiel');
        $this->addSql('DROP TABLE departement');
        $this->addSql('DROP TABLE detail_materiel');
        $this->addSql('DROP TABLE materiel');
        $this->addSql('DROP TABLE qty_updated');
        $this->addSql('DROP TABLE sortie');
        $this->addSql('DROP TABLE type_materiel');
        $this->addSql('DROP TABLE user');
    }
}
