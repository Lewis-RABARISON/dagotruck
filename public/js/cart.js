$(document).ready(function () {
    $("#save").attr("disabled",true);
    fetchClientName();
    selectPaymentType();
    chooseDateEcheance();
    savePurchase();
    timeOut();
    removeAllCart();
    bootstrapValidateField("#type-paiement", "required", "Veuillez selectionner un type de paiement");
});

/* recupère le nom du client */
function fetchClientName() { 
    $(document).on("input",".client-name",function(e) { 
        $("[name=nom]").val(e.target.value);
        if ($(this).val() != "" && $(".type-paiement").val() == "--Veuillez selectionner--") {
            $("#save").attr("disabled",true);
        }
        if ($(this).val() != "" && $(".type-paiement").val() != "--Veuillez selectionner--") {
            $("#save").attr("disabled",false);
        }
    });
}

/* Choisit la date d'echeance */
function chooseDateEcheance() { 
    $(document).on("change",".date-echeance",function(e) { 
        $("[name=date-echeance]").val(e.target.value);
    });
}

/* selectionne le type de paiement */
function selectPaymentType() { 
    $(document).on("change",".type-paiement",function(e) { 
        e.preventDefault();
        if ($(this).val() == "Abonné") {
            $(".avance-echeance-achat").removeClass("d-none");
            $(".hr-cart").removeClass("d-none");
            $("[name=type-paiement]").val(e.target.value);
            $("#save").removeAttr("disabled");
            if ($(".client-name").val() == "" && $(this).val() == "--Veuillez selectionner--") {
                $("#save").attr("disabled",true);
                bootstrapValidateField("#type-paiement", "required", "Veuillez selectionner un type de paiement");
            } else {
                $("#save").attr("disabled",false);
            }
        }
        else{
            $(".avance-echeance-achat").addClass("d-none");
            $("[name=type-paiement]").val(e.target.value);
            $("[name=avance]").val("");
            $("[name=date-echeance]").val("");
            $(".hr-cart").addClass("d-none");
            $("#save").removeAttr("disabled");
            if ($(".client-name").val() == "") {
                $("#save").attr("disabled",true);
            } else {
                $("#save").attr("disabled",false);
            }
        }
    });
}

/* Enregistrer l'achat */
function savePurchase() { 
    $("#save").click(function (e) { 
        e.preventDefault();
        var materiel_id = [];
        var quantity = [];
        var nom = [];
        var type_paiement = [];
        var avance = [];
        var date_echeance = [];
        $(".materiel").each(function () {
            materiel_id.push($(this).val());
        }); 
        $(".quantity").each(function () {
            quantity.push($(this).val());
        }); 
        $(".nom").each(function () {
            nom.push($(this).val());
        });
        $(".type-paiement").each(function () {
            type_paiement.push($(this).val());
        }); 
        $(".avance").each(function () {
            avance.push($(this).val());
        }); 
        $(".date-echeance-cart").each(function () {
            date_echeance.push($(this).val());
        }); 
        $.ajax({
            type: "POST",
            url: url,
            data: {materiel_id:materiel_id,quantity:quantity,nom:nom,type_paiement:type_paiement,avance:avance,date_echeance:date_echeance},
            beforeSend: function () {
                $("#save").attr("disabled",true);
                $("#save").html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Enregistrement...`);
            },
            success: function (response){
                if(response.success) {
                    window.location.replace(url_list_achat);
                }
            },
            error: function (error) { 
                $(".notify-purchase").after('<div class="alert bg-danger text-white">'+
                    "<i class='fas fa-times'></i> Votre achat n'a pas été enregistré"+
                    '</div>'
                );
                /*$(".avance").val("");
                $("[name=type-paiement]").val("");
                $("[name=avance]").val("");
                $(".date-echeance").val("YYYY-MM-DD");
                $("[name=nom]").val("");*/
                timeOut();
            }
        });
    });
}

/* time out notification */
function timeOut(){
    setTimeout(() => {
        $('.alert').fadeOut('slow');
    }, 2000);
}

/* supprimer l'achat dans le panier */
function removeAllCart() { 
    $(".btn-remove-all-cart").click(function (e) { 
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: url_remove_purchase,
            dataType: "json",
            success: function (response) {
                if (response.success) {
                    $(".vos-achats").html(`<div class="d-flex justify-content-center bg-info p-4 mt-4">
                                                <h2 class="mt-4 text-white"><b>Aucun achat<b></h2>
                                            </div>`);
                }
            },
            error: function (error){
                alert("erreur suppression de tous les achats");
            }
        });
    });
}

/* bootstrap valide le champs */
function bootstrapValidateField(selector, type, message) { 
    bootstrapValidate(selector, type + ":" + message);
}