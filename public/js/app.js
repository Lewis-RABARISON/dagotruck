$(document).ready(function(){
    $(".select-two").select2();
    /*$(window).scroll(function () { 
        $("header nav").toggleClass("fixed-top animated slideInDown shadow", $(this).scrollTop() > 170);
    });*/

    datatable();
    bootstrapValidateField("#client-name", "required", "Le nom du client est obligatoire");
    bootstrapValidateField("#date-echeance", "required", "La date-echeance est obligatoire");
    bootstrapValidateField("#date-echeance", "ISO8601", "La date-echeance n'est pas une format valide");
    /*bootstrapValidateField("#type-paiement", "value:--Veuillez selectionner--", "Veuillez selectionner un type de paiement");*/
    bootstrapValidateField("#avance-achat", 'min:0:', "L'avance est au minimum 0");
    timeOut();
});

/* datatable */
function datatable() { 
    let datatable = $('#table').DataTable({
        "language": {
            "search": "Rechercher",
            "lengthMenu": "Afficher _MENU_ enregistrements par page",
            "zeroRecords": "Aucune correspondance",
            "info": "Affichage page _PAGE_ de _PAGES_",
            "infoEmpty": "Aucune correspondance disponible",
            "infoFiltered": "(filté de _MAX_ enregistrements)",
            "paginate": {
                "previous": "Précédent",
                "next": "Suivant"
            }
        }
    });

    let datatable_search = $(".research-datatable");
    datatable_search.on("input",function(){
        let datatable_search_value = datatable_search.val();
        let is_numeric = new RegExp("^[0-9]*$");
        let value_thousand = datatable_search_value.replace(/\s+/g,"");
        if (is_numeric.test(value_thousand)) {
            datatable.search(value_thousand).draw();
        } else {
            datatable.search(datatable_search_value).draw();
        }
    });
}

/* bootstrap valide le champs */
function bootstrapValidateField(selector, type, message) { 
    bootstrapValidate(selector, type + ":" + message);
}

/* timeOut message d'alert */
function timeOut(){
    setTimeout(() => {
        $('.alert').fadeOut('slow');
    }, 2000);
}

timeOut();