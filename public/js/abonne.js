$(document).ready(function () {
    function saveMoreAbonne()
    {
        $(".btn-save-more-abonne").click(function (e) { 
            e.preventDefault();
            var nouveau_abonnement = $(".form-abonne-item").clone();
            nouveau_abonnement.removeClass("d-none");
            nouveau_abonnement.find(".btn-remove-abonne").removeClass("d-none");
            var key = parseInt($(this).data('key')) + 1;
            $(this).data('key', key);
            nouveau_abonnement.data('key', $(this).data('key'));
            var block_abonne = `<div class="form-abonne-item" id="abonne_${key}" data-key=${key}>${nouveau_abonnement.html()}</div>`;
            $(".form-abonne").append(block_abonne);
            removeAbonnement();
        });
    }
    saveMoreAbonne();
    function removeAbonnement()
    {
        var btn_remove_abonne = $(".btn-remove-abonne");
        btn_remove_abonne.click(function (e) { 
            e.preventDefault();
            var key = parseInt($(this).parent().data('key'));
            $("#abonne_"+key).remove();
        });
    }
    removeAbonnement();

    $(".echeance").change(function (e) { 
        e.preventDefault();
        $(".date-echeance").val($(this).val());
    });

    $(".btn-save-abonne").click(function (e) { 
        e.preventDefault();
        var self = $(this);
        var materiel_id   = [];
        var quantity      = [];
        var avance        = [];
        var nom_acheteur  = [];
        var code_facture  = [];
        var date_echeance = [];

        $(".materiel").each(function () {
            materiel_id.push($(this).val());
        }); 
        $(".quantity").each(function () {
            quantity.push($(this).val());
        }); 
        $(".avance").each(function () {
            avance.push($(this).val());
        }); 
        $(".nom-acheteur").each(function () {
            nom_acheteur.push($(this).val());
        }); 
        $(".code-facture").each(function () {
            code_facture.push($(this).val());
        }); 
        $(".date-echeance").each(function () {
            date_echeance.push($(this).val());
        }); 

        $.ajax({
            type: "POST",
            url: url_add_abonne,
            data: {materiel_id:materiel_id,quantity:quantity,avance:avance,nom_acheteur:nom_acheteur,code_facture:code_facture,date_echeance:date_echeance},
            dataType: "json",
            beforeSend: function () {
                $(".btn-save-abonne").attr("disabled",true);
                $(".btn-save-abonne").html(`<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                Enregistrement...`);
            },
            success: function (response) {
                if (response.success) {
                    /* self.parentsUntil(".form-abonne").children().remove(); */
                    /*self.parents(".form-abonne").prev().remove();*/
                    $(".notification-add-abonne").html(`<div class="alert bg-success text-white"><i class="fas fa-check"></i> ${response.message}</div>`);
                    window.location.replace(absolute_url_abonne);
                }
            },
            error: function (error){
                alert("error");
            }
        });
    });
    function timeOut(){
        setTimeout(() => {
            $('.alert').fadeOut('slow');
        }, 2000);
    }
});

$(".error-quantity").hide();
$(".error-avance").hide();

$(document).on("input",".quantity",function (event) { 
    if ($(event.target).val() < 0) {
        $(".error-quantity").show();
        $(".btn-save-abonne").attr("disabled",true);
    } 
});

$(document).on("blur",".quantity",function (event) { 
    if ($(event.target).val() > 0) {
        $(".error-quantity").hide();
        $(".btn-save-abonne").attr("disabled",false);
    } 
});

$(document).on("input",".avance",function (event) { 
    if ($(event.target).val() < 0) {
        $(".error-avance").show();
        $(".btn-save-abonne").attr("disabled",true);
    } 
});

$(document).on("blur",".avance",function (event) { 
    if ($(event.target).val() > 0) {
        $(".error-avance").hide();
        $(".btn-save-abonne").attr("disabled",false);
    } 
});