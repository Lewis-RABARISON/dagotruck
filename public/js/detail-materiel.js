$(document).ready(function(){
    materialDetailMaterialType();
    addCart();
    timeOut();
    inputResearch();


    /*filter*/
    $('.dago-truck-filter .dago-clik').each(function(){
        class_filter = $(this).data('slug');
        $(this).click(function(){
            $('.table-filter').show();
            $('.table-filter').fadeOut();
            $('.table-filter').filter('.' + class_filter).fadeIn(200);
        });
    });
});

/* type de matériel du détail matériel */
function materialDetailMaterialType() { 
    $(document).on("change","#detail_materiel_typeMateriel",function() { 
        let type_materiel_id = $(this).val();
        var url = "/detail-materiel/" + type_materiel_id + "/materiel";
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            success: function(response){
                if (response.success) {
                    var options = "<option>--Veuillez selectionner--</option>";
                    for (let i = 0; i < response.type_materiel_id.length; i++) {
                        options += `<option value=${response.type_materiel_id[i].id}>
                                        ${response.type_materiel_id[i].nom_marque}
                                    </option>`;
                    }
                    $("#detail_materiel_materiel").html(options).select2();
                }
            },
            error: function(error){
                $("#detail_materiel_materiel").html("<option>Aucune correspondance</option>").select2();
            }
        })
    });
}

/* ajout le détail matériel */
function addCart() { 
    $(".btn-add-cart").click(function(e){ 
        e.preventDefault();
        var url = $(this).data("url");
        $('.alert').remove();
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            success: function (response) {
                console.log(response)
                if (response.success) {
                    $(".notify-add-cart").before(`<div class="alert bg-success text-white"><i class="fas fa-check"></i> ${response.message}</div> `);
                    timeOut();
                } else {
                    $(".notify-add-cart").before('<div class="alert bg-danger text-white">'+
                        "<i class='fas fa-times'></i> L'achat n'a pas été ajouté dans le panier"+
                        '</div>'
                    );
                    timeOut();
                }
            }
        });
    });
}

/* timeOut message d'alert */
  function timeOut(){
    setTimeout(() => {
        $('.alert').fadeOut('slow');
    }, 2000);
}

/* recuperer valeur à rechercher */
function inputResearch() {
    $(document).on("blur",".search-detail-materiel",function (e) {
        e.preventDefault();
        let value_search_detail_materiel = $(this).val();

        /*$("#table-detail-materiel tbody tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value_search_detail_materiel) > -1);
        });*/

        $.ajax({
            type: "GET",
            url: path_material_detail,
            data: {'q': value_search_detail_materiel},
            dataType: "json",
            beforeSend: function () {
                $(".table-body-detail-materiel").html(`
                <tr>
                    <td colspan="7" align="center">
                        <div class="spinner-border spinner-border-detail-materiel text-info" role="status">
                            <span class="sr-only">Loading...</span>
                        </div>
                    </td>
                </tr>
            `);
            },
            success: function (response) {
                if (response.success){
                    $(".table-body-detail-materiel").html(response.content);
                }
            },
            error: function (error) {
                console.log("erreur");
            }
        });
    });
}
