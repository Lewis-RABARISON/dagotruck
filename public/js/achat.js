$(document).ready(function () {
    $(".achat-journalier").hide();
    $(".achat-mensuel").hide();
    $(".achat-annuel").hide();
    $(".btn-tri-achat").hide();
    $(".btn-actualiser-tri").hide();
    TypePurchase();
    sortingPurchase();
});

/* type d'achat */
function TypePurchase() { 
    let tri_achat = $(".tri-achat");
    let achat_journalier = $(".achat-journalier");
    let achat_mensuel = $(".achat-mensuel");
    let achat_annuel = $(".achat-annuel");

    tri_achat.change(function (e) { 
        e.preventDefault();
        if ($(this).val() == "Achat journalière") {
            achat_journalier.show();
            achat_mensuel.hide();
            achat_annuel.hide();
            $(".btn-tri-achat").show();
            $(".btn-actualiser-tri").show();
            $("#form-sorting-purchase").attr("data-url",PATH_SORTING_PURCHASE_IN_DAY);
        } else if ($(this).val() == "Achat mensuel") {
            achat_journalier.hide();
            achat_mensuel.show();
            $(".btn-tri-achat").show();
            $(".btn-actualiser-tri").show();
            $("#form-sorting-purchase").attr("data-url",PATH_SORTING_PURCHASE_IN_MONTH);
        } else if ($(this).val() == "Achat annuel") {
            achat_journalier.hide();
            achat_mensuel.hide();
            achat_annuel.show();
            $(".btn-tri-achat").show();
            $(".btn-actualiser-tri").show();
            $("#form-sorting-purchase").attr("data-url",PATH_SORTING_PURCHASE_IN_YEAR);
        } else {
            achat_journalier.hide();
            achat_mensuel.hide();
            achat_annuel.hide();
            $(".btn-tri-achat").hide();
            $(".btn-actualiser-tri").hide();
        }
    });
}

/* Trie l'achat */
function sortingPurchase() { 
    $("#form-sorting-purchase").each(function () {
        $(this).submit(function (e) { 
            e.preventDefault();
            let data = $(this).serializeArray();
            let data_achat_journalier = data[0].value;
            let data_achat_mensuel = data[1].value;
            let data_achat_annuel = data[2].value;

            var url = $(this).data("url");
            /*console.log(url);*/
            var data_to_send = {};

            if (data_achat_journalier != 0) {
                var data_to_send = {data_achat_journalier:data_achat_journalier};
            } else if (data_achat_annuel != 0) {
                var data_to_send = {data_achat_annuel:data_achat_annuel};
            } else if (data_achat_mensuel != 0) {
                var data_to_send = {data_achat_mensuel:data_achat_mensuel};
            }

            $.ajax({
                type: "post",
                url: url,
                data: data_to_send,
                dataType: "json",
                success: function (response) { 
                    if (response.success) {
                        let row = "";
                        let data = response.data;
                        for (let i = 0; i < data.length; i++) {
                            row += `
                                    <tr>
                                        <td>${data[i].nom_acheteur}</td>
                                        <td>${data[i].nom_materiel}</td>
                                        <td>${data[i].type_paiement}</td>
                                        <td>${data[i].nom_departement}</td>
                                        <td>${data[i].date_achat}</td>
                                        <td>${data[i].qte_achete}</td>
                                        <td>${data[i].prix_unitaire}</td>
                                        <td>${data[i].qte_achete * data[i].prix_unitaire}</td> Fmg
                                        <td>${data[i].avance}</td>
                                        <td>${data[i].qte_achete * data[i].prix_unitaire - data[i].avance}</td>
                                        <td>${data[i].date_echeance}</td>
                                        <td>
                                            <a href="/achat/${data[i].achat_id}/edit" class="btn btn-sm btn-info px-4 btn_rounded">Editer</a>
                                            <form method="post" action="/achat/${data[i].achat_id}/delete" class="d-inline-block" onsubmit="return confirm('Vous voulez vraiment supprimer');">
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button class="btn btn-sm btn-danger px-4 btn_rounded" role="button">Supprimer</button>
                                            </form>
                                        </td>
                                    </tr>
                                `;
                        }
                        $("tbody").html(row);
                    }
                },
                error: function (error) { 
                    alert("Veuillez selectionner");
                }
            });

        });
    });
}







